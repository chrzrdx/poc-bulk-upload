from flask import Flask, render_template, abort
import random

app = Flask(__name__)


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/ping")
def ping():
    if random.random() > 0.5:
        return "PONG"
    else:
        abort(404)


if __name__ == "__main__":
    app.run(debug=True)
