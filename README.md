# Batching promises (bulk upload on single API endpoint with retry functionality)

- Setup directory with `bash setup.sh`
- Activate virtual environment if necessary with `source venv/bin/activate`
- Run flask app with `python app.py`
- Navigate to `http://localhost:5000`

